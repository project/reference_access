<?php

namespace Drupal\reference_access\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Reference access settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'reference_access_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['reference_access.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('reference_access.settings');
    $content_types = \Drupal::service('entity_type.manager')
      ->getStorage('node_type')
      ->loadMultiple();

    $form['check_content_types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Content types to restrict'),
      '#description' => $this->t('Content types that should be checked for reference access. Nodes that are not chosen will be ignored by this module (allowed access), while nodes ticked will have to be referenced in some way depending on the rules chosen below.'),
      '#default_value' => $config->get('check_content_types'),
    ];
    foreach ($content_types as $name => $content_type) {
      $form['check_content_types']['#options'][$name] = $content_type->label();
    }

    $form['check_direct_ref_nodes'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Check nodes directly referenced by users'),
      '#description' => $this->t('For example: user -> node.'),
      '#default_value' => $config->get('check_direct_ref_nodes') ?: true, // Defaults to true
    ];
    $form['check_indirect_ref_nodes'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Check nodes referenced by nodes that are referenced by users'),
      '#description' => $this->t('For example: user -> node -> node.'),
      '#default_value' => $config->get('check_indirect_ref_nodes'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  // phpcs:ignore
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('reference_access.settings')
      ->set('check_content_types', $form_state->getValue('check_content_types'))
      ->set('check_direct_ref_nodes', $form_state->getValue('check_direct_ref_nodes'))
      ->set('check_indirect_ref_nodes', $form_state->getValue('check_indirect_ref_nodes'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
